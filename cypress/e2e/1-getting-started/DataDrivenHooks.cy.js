///<reference types="Cypress"/>


describe("Casos de prueba avanzados", function () {
    before(() => {
        cy.fixture('Ejemplos Hook').then(function (datos) {
            //acercion "this." se utiliza para habilitar un objeto fuera del vloque donde fue creado
            this.datosGral = datos
            //hace disponible la imagen que se encuentra subida a la carpeta
            cy.fixture(this.datosGral.image).as('image')
        })
        
    })
    /* This gets the same indentation level as your "it" blocks, nested directly under "describe".
       It will cause cypress to ignore all uncaught JS exceptions.*/
    Cypress.on('uncaught:exception', (err, runnable) => {
        return false;
    })
    beforeEach(() => {
        //ingresamos la URL
        cy.visit('https://demoqa.com/automation-practice-form')
    })
    it('llenamos primer formulario utilizando data', function () {
        cy.get('#firstName').type(this.datosGral.name)
        cy.get('#lastName').type(this.datosGral.lastName)
        cy.get('#userEmail').type(this.datosGral.email)
        cy.get('input[name="gender"][value="' + this.datosGral.sex + '"]').click({ force: true }).should('be.checked')
        cy.get('#userNumber').type(this.datosGral.tel)
        cy.get('#dateOfBirthInput').click()
        cy.get('.react-datepicker__month-select').should('be.visible').select(this.datosGral.bDay[0])
        cy.get('.react-datepicker__year-select').should('be.visible').select(this.datosGral.bDay[1])
        cy.get('.react-datepicker__day--0' + this.datosGral.bDay[2]).should('be.visible').click()
        cy.get('#dateOfBirthInput')
            .should('contain.value', this.datosGral.bDay[0].substring(0, 3))
            .should('contain.value', this.datosGral.bDay[1])
            .should('contain.value', this.datosGral.bDay[2])
        cy.get('.subjects-auto-complete__value-container').type(this.datosGral.subject)
        cy.get('div[id^=react-select-').click()
        cy.get('.subjects-auto-complete__value-container').should('contain.text', this.datosGral.subject)
        cy.get('div[class="custom-control custom-checkbox custom-control-inline"]:has(label:contains("' + this.datosGral.hobbies[0] + '")) input')
            .check({ force: true }).should('be.checked')
        cy.get('div[class="custom-control custom-checkbox custom-control-inline"]:has(label:contains("' + this.datosGral.hobbies[1] + '")) input')
            .check({ force: true }).should('be.checked')
        cy.get('#uploadPicture').then(function($el){
            //convertir imagen a string base64 siempre es igual en cypress
            const blob = Cypress.Blob.base64StringToBlob(this.image, 'ímage/jpeg')

            const file = new File([blob],this.datosGral.image,{type: 'image/jpeg'})
            const lista = new DataTransfer()
            lista.items.add(file)
            const myFileList = lista.files
            $el[0].files = myFileList
            $el[0].dispatchEvent(new Event('change',{bubbles:true}))

        })
       

    })
    it('llenamos primer formulario utilizando data', function () {
        cy.get('#firstName').type(this.datosGral.name)
        cy.get('#lastName').type(this.datosGral.lastName)
        cy.get('#userEmail').type(this.datosGral.email)
        cy.get('input[name="gender"][value="' + this.datosGral.sex + '"]').click({ force: true }).should('be.checked')
        cy.get('#userNumber').type(this.datosGral.tel)
        cy.get('#dateOfBirthInput').click()
        cy.get('.react-datepicker__month-select').should('be.visible').select(this.datosGral.bDay[0])
        cy.get('.react-datepicker__year-select').should('be.visible').select(this.datosGral.bDay[1])
        cy.get('.react-datepicker__day--0' + this.datosGral.bDay[2]).should('be.visible').click()
        cy.get('#dateOfBirthInput')
            .should('contain.value', this.datosGral.bDay[0].substring(0, 3))
            .should('contain.value', this.datosGral.bDay[1])
            .should('contain.value', this.datosGral.bDay[2])
        cy.get('.subjects-auto-complete__value-container').type(this.datosGral.subject)
        cy.get('div[id^=react-select-').click()
        cy.get('.subjects-auto-complete__value-container').should('contain.text', this.datosGral.subject)
        cy.get('div[class="custom-control custom-checkbox custom-control-inline"]:has(label:contains("' + this.datosGral.hobbies[0] + '")) input')
            .check({ force: true }).should('be.checked')
        cy.get('div[class="custom-control custom-checkbox custom-control-inline"]:has(label:contains("' + this.datosGral.hobbies[1] + '")) input')
            .check({ force: true }).should('be.checked')
        cy.get('#uploadPicture').then(function($el){
            //convertir imagen a string base64 siempre es igual en cypress
            const blob = Cypress.Blob.base64StringToBlob(this.image, 'ímage/jpeg')

            const file = new File([blob],this.datosGral.image,{type: 'image/jpeg'})
            const lista = new DataTransfer()
            lista.items.add(file)
            const myFileList = lista.files
            $el[0].files = myFileList
            $el[0].dispatchEvent(new Event('change',{bubbles:true}))

        })
        cy.get('#currentAddress').type(this.datosGral.direccion)
        cy.get('#state').click()
        cy.get('div[id*=react-select]').contains(this.datosGral.estado).click()
        cy.get('#city').click()
        cy.get('div[id*=react-select]').contains(this.datosGral.ciudad).click()

    })
})