///<reference types="Cypress"/>

describe("caso de prueba Sils", function () {

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false;
    })

    beforeEach(() => {
        cy.fixture('prueba sils').then(function (datos) {
            //acercion "this." se utiliza para habilitar un objeto fuera del vloque donde fue creado
            this.datosGral = datos
            cy.visit('sils.com.ar/#/login?isFullLogin=true')

        })
    })
    it('login', function () {
        cy.url().should('contain', '#/login?isFullLogin=true')
        cy.get('#username').type(this.datosGral.user)
        cy.get('#password').type(this.datosGral.pass)
        cy.get('.btn').click()
    })
    it('Primer test sils', function () {
        cy.url().should('contain', '#/login?isFullLogin=true')
        cy.get('#username').type(this.datosGral.user)
        cy.get('#password').type(this.datosGral.pass)
        cy.get('.btn').click()
        cy.wait(30000)
        cy.url().should('contain', '#/dashboard')
        cy.get('.sidebar > .sidebar-nav > .ps-container > .nav > :nth-child(3) > .nav-link').click()
        cy.wait(3000)
        cy.url().should('contain', '#/reportes')
        cy.get(':nth-child(1) > :nth-child(1) > .card > .card-body > .card-footer > .btn').click()
        cy.get('.mr-3 > .input-group > .form-control').type(this.datosGral.dominio)
        cy.get('.d-flex > :nth-child(1) > [data-v-99b976ee=""] > :nth-child(1) > .btn').click()
        cy.get(':nth-child(1) > [data-v-99b976ee=""] > #dateTimeInput > #dateRow > .col > .vc-container > .vc-w-full > .grid > .vc-grid-cell-row-1.vc-grid-cell-row--1 > .vc-pane > .vc-header > .vc-title-layout > .vc-title-wrapper > .vc-title')
            .should('contain.text', "" + this.datosGral.fechaDesde[2] + " " + this.datosGral.fechaDesde[3] + "").click()
        cy.get('span [aria-label="' + this.datosGral.fechaDesde[4] + ' 2000"]').click()
        cy.get(':nth-child(1) > [data-v-99b976ee=""] > #dateTimeInput .id-'+this.datosGral.fechaHasta[3]+'-'+this.datosGral.fechaHasta[1]+'-'+this.datosGral.fechaHasta[0]+'').click()
        cy.get('.d-flex > :nth-child(2) > [data-v-99b976ee=""] > :nth-child(1) > .btn').click()
        cy.get(':nth-child(2) > [data-v-99b976ee=""] > #dateTimeInput > #dateRow > .col > .vc-container > .vc-w-full > .grid > .vc-grid-cell-row-1.vc-grid-cell-row--1 > .vc-pane > .vc-header > .vc-title-layout > .vc-title-wrapper > .vc-title')
            .should('contain.text', "" + this.datosGral.fechaHasta[2] + " " + this.datosGral.fechaHasta[3] + "").click()
        cy.get(':nth-child(2) > span [aria-label="' + this.datosGral.fechaHasta[4] + ' 2000"]').click()
        cy.get(':nth-child(2) > [data-v-99b976ee=""] > #dateTimeInput .id-'+this.datosGral.fechaHasta[3]+'-'+this.datosGral.fechaHasta[1]+'-'+this.datosGral.fechaHasta[0]+'').click()
        cy.get(':nth-child(2) > [data-v-99b976ee=""] > #dateTimeInput > #closeRow > a').should("be.visible").click()
        cy.get(':nth-child(5) > .btn').click()
    })
})



