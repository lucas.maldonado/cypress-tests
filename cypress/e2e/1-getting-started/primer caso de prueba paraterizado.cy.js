///<reference types="Cypress"/>
//suite casos de prueba que contiene cada caso
describe('primera suite de casos de prueba/ primer conjunto de casos de prueba',function()
{
    //caso 1
it('Contabilizar la cantidad de elementos en la pagina principal', function(){
    //paso 1
    //paso 2
    //paso...
    // ingresamos a la pagina
    cy.visit("https://automationpractice.com/index.php")
    //verificar la cantidd de elementos visibles/disponibles
    cy.get('#homefeatured .product-container').should('have.length', 7)
    //Obtenemos el elemento homefeatured .product-container como parametro
    cy.get('#homefeatured .product-container').as('ProductosPopulares')
    //Verificamos nuevamente la cantidad de elementos utilizando el parametro
    cy.get('@ProductosPopulares').should('have.length',7)
}
)

    //caso 2
it('Agregar al carrito el elemento tipo "Blouse" desde la pagina principal', function(){
// ingresamos a la pagina
cy.visit("https://automationpractice.com/index.php")
//Obtenemos el elemento homefeatured .product-container como parametro
cy.get('#homefeatured .product-container').as('ProductosPopulares')
//iteramos par encontrar un producto con nombre "X"
cy.get('@ProductosPopulares')
.find('.product-name')
.each(($el,index,$list) => {
    if($el.attr('title')==='Blouse'){
            cy.log('Se ha encontrado el elemento deseado')
            cy.get('@ProductosPopulares').eq(index).contains('Add to cart').click()
    }

})

}
)
    //caso 3
}
)

    
