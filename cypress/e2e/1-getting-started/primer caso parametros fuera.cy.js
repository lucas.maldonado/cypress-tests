///<reference types="Cypress"/>
//suite casos de prueba que contiene cada caso
describe('primera suite de casos de prueba/ primer conjunto de casos de prueba',function()
{
    beforeEach(()=>{
        // ingresamos a la pagina
        cy.visit("https://automationpractice.com/index.php")
    })

    //caso 1
it('Contabilizar la cantidad de elementos en la pagina principal', function(){
    //paso 1
    //paso 2
    //paso...
    //verificar la cantidd de elementos visibles/disponibles
    cy.get('#homefeatured .product-container').should('have.length', 7)
    //Obtenemos el elemento homefeatured .product-container como parametro
    cy.get('#homefeatured .product-container').as('ProductosPopulares')
    //Verificamos nuevamente la cantidad de elementos utilizando el parametro
    cy.get('@ProductosPopulares').should('have.length',7)
}
)

    //caso 2
it('Agregar al carrito el elemento tipo "Blouse" desde la pagina principal', function(){
    //Obtenemos el elemento homefeatured .product-container como parametro
    cy.get('#homefeatured .product-container').as('ProductosPopulares')
    //iteramos par encontrar un producto con nombre "X"
    cy.get('@ProductosPopulares')
    .find('.product-name')
    .each(($el,index,$list) => {

        cy.get('@ProductosPopulares').eq(index).find('.price').then(function($el1){
            let precio = $el1.text()
            cy.log(precio)


        if($el.attr('title')==='Printed Dress'&& precio.includes('26.00')){
            cy.log('Se ha encontrado el elemento deseado')
            cy.log('Se ha encontrado el precio deseado')
            cy.get('@ProductosPopulares').eq(index).contains('Add to cart').click()
            }
    
        })

    })
    cy.get('h2 > .ajax_cart_product_txt')
        .should('contain.text','There is 1 item in your cart.')
        .should('be.visible')
    })
    
    //caso 3
    it('Verificacion que el drop down "WOMEN" posee los elementos necesarios', function(){

        //flotamos sobre un elemento
        cy.get('#block_top_menu > ul > li:nth-child(1) > ul').invoke('attr', 'style', 'display: block')
        cy.get('a[title="Tops"]').should('be.visible')
        cy.get('a[title="T-shirts"]').should('be.visible')
        cy.get('a[title="Blouses"]').should('be.visible')
        cy.get('a[title="Dresses"]').should('be.visible')
        cy.get('a[title^="Casual"]').should('be.visible')
        cy.get('a[title^="Evening"]').should('be.visible')
        cy.get('a[title^="Summer"]').should('be.visible')

    })
    //caso 4
    it('Verificar que los checkboxes estan funcionando', function () {
        cy.get('.sf-menu > :nth-child(2) > .sf-with-ul').click()
        cy.get('li[class="nomargin hiddable col-lg-6"]:has(a[href*="categories-casual_dresses"]) input').check().should('be.checked')
        cy.get('li[class="nomargin hiddable col-lg-6"]:has(a[href*="categories-evening_dresses"]) input').should('not.be.checked')
        cy.get('li[class="nomargin hiddable col-lg-6"]:has(a[href*="categories-summer_dresses"]) input').should('not.be.checked')
    })
})

    
