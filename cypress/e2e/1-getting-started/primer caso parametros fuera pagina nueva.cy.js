///<reference types="Cypress"/>
//suite casos de prueba que contiene cada caso
describe('casos de prueba con pagina diferente a la de el tutorial', function () {
    beforeEach(() => {
        // ingresamos a la pagina
        cy.visit("https://magento.softwaretestingboard.com/")
    })

    
        //caso 1
        it('Verificacion que el drop down "GEAR" posee los elementos necesarios', function () {
    
            //flotamos sobre un elemento
            cy.get('#ui-id-2 > li.level0.nav-4.category-item.level-top.parent.ui-menu-item > ul').invoke('attr', 'style', 'display: block')
            cy.get('#ui-id-25').should('be.visible')
            cy.get('#ui-id-26').should('be.visible')
            cy.get('#ui-id-27').should('be.visible')
    
    
        })
        //caso 2
        it('Verificar que el dropdown funcione ', function () {
    
            cy.get('#ui-id-2 > li.level0.nav-4.category-item.level-top.parent.ui-menu-item > ul').invoke('attr', 'style', 'display: block')
            cy.get('#ui-id-27').click()
            cy.get(':nth-child(3) > .toolbar-sorter > #sorter').select('Price').should('have.value', 'price')
    
        })

    it('Caso completode compra', function () {

        cy.get('.panel > .header > .authorization-link > a').click()
        cy.get('#email').type('pruebacypress@test.com')
        cy.get('.login-container > .block-customer-login > .block-content > #login-form > .fieldset > .password > .control > #pass').type("Test1234'")
        cy.get('.login-container > .block-customer-login > .block-content > #login-form > .fieldset > .actions-toolbar > div.primary > #send2 > span').click()
        cy.get('#search').type('Mens Tees')
        cy.get('#search_mini_form > div.actions > button').click()
        cy.get('.wrapper > .products > li').as('items')
        cy.get('@items').should('have.length', 3)
        cy.get('li > .product-item-info > .details > .name').as('buy')
        cy.get('@buy')
            .each(($el, index, $list) => {

                cy.get('@items').eq(index).find('.price').then(function ($el1) {
                    let precio = $el1.text()
                    cy.log(precio)
                    if ($el.contents('Deion Long-Sleeve EverCool™ Tee') && precio.includes('$39.00')) {
                        cy.log('Se ha encontrado el elemento deseado')
                        cy.log('Se ha encontrado el precio deseado')
                        cy.get('@items').eq(index).contains('Add to Cart').click({ force: true })
                    }

                })

            })
        /*cy.get('.showcart > .counter').click()
        cy.get('#top-cart-btn-checkout').click()
        cy.get('#DVJINUF').type('Prueba')*/
        
    })

})
